Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: goattracker
Upstream-Contact: Lasse Öörni <loorni@gmail.com>
Source: https://cadaver.github.io/tools.html
Files-Excluded: win32/* src/bme/*.exe

Files: *
Copyright: 1998-2021 Lasse Öörni <loorni@gmail.com>
           2007-2009 Antti S. Lankila
           Téli Sándor (HardSID 4U)
License: GPL-2-or-later

Files: src/asm/* without asmtab.*
Copyright: 2002-2005 Magnus Lind (6510 crossassembler)
License: BSD-4-clause
 This software is provided 'as-is', without any express or implied warranty.
 In no event will the authors be held liable for any damages arising from
 the use of this software.
 .
 Permission is granted to anyone to use this software, alter it and re-
 distribute it freely for any non-commercial, non-profit purpose subject to
 the following restrictions:
 .
   1. The origin of this software must not be misrepresented; you must not
      claim that you wrote the original software. If you use this software in a
      product, an acknowledgment in the product documentation would be
      appreciated but is not required.
 .
   2. Altered source versions must be plainly marked as such, and must not
      be misrepresented as being the original software.
 .
   3. This notice may not be removed or altered from any distribution.
 .
   4. The names of this software and/or it's copyright holders may not be
      used to endorse or promote products derived from this software without
      specific prior written permission.

Files: src/asm/asmtab.*
Copyright: 1984,1989,1990,2000-2005 Free Software Foundation, Inc.
License: GPL-2-or-later

Files: src/resid/*
Copyright: 2004 Dag Lem <resid@nimrod.no> (reSID engine)
License: GPL-2-or-later

Files: debian/*
Copyright:
        2007-2024 Alex Myczko <tar@debian.org>
        2019 Olivier Humbert <trebmuh@tuxfamily.org>
License: GPL-2-or-later

License: GPL-2-or-later
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
